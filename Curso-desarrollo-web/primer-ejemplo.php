<!DOCTYPE html>
<html>
<head>
	<title>Primer ejemplo PHP</title>
</head>
<body>

<?php
	// Es para comentarios de una línea

	/*
		Es para comentarios de 
		barias 
		lineas
	*/

	// DECLARACIONES DE VARIABLES

	// Ejemplo:	$nombreDeVariable
	// TODAS las variables en PHP empiezan con el signo "$"	

	$nombre = "Daniel Quisbert";

	echo "<h1>" . $nombre."</h1>";

?>

</body>
</html>
